const nanoid = require('nanoid');
const bcrypt = require('bcrypt');
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

const adapter = new FileSync('db.json');
const db = low(adapter);

module.exports = (app) => {

  // Sign-in
  app.post("/api/login", (req, res) => {
    const { username, password } = req.body;

    const user = db.get('users')
      .find({ username })
      .value();
      if (!user) {
        return res.status(404).send({
            message: "Account not found"
        });
      }
      const match = bcrypt.compareSync(password, user.password);
      if(match){
        return res.status(200).send({
            error: false,
            username: user.username
          });
      }
      else{
        return res.status(401).send({
          message: "Wrong password"
        });
      }
  });

  // Get all users
  app.get(`/api/users`, async (req, res) => {
    const users = db.get('users').value();
    return res.status(200).send(users);
  });

  // Get user by username
  app.get(`/api/user/:username`, async (req, res) => {
    const { username } = req.params;

    const user = db.get('users')
      .find({ username })
      .value();

    return res.status(200).send({
      user
    });
  });

  // Post new user
  app.post(`/api/user`, async (req, res) => {
    let { username, email, password } = req.body;
    const id = nanoid.nanoid();

    bcrypt.hash(password, 10, (err, hash) => {
        if (err) {
          console.error(err);
          return;
        }
        password = hash;

        db.get('users')
        .push({ id, username, email, password })
        .write();

        const user = db.get('users')
        .find({ id })
        .value();

        return res.status(201).send({
        error: false,
        user
        });
    });
  });

  // Edit user
  app.put(`/api/user`, async (req, res) => {
    const { id, username, email, password } = req.body;

    db.get('users')
        .find({ id })
        .assign({ username, email, password })
        .write();

    const user = db.get('users')
      .find({ id })
      .value();

    return res.status(202).send({
      error: false,
      user
    });
  });

  // Delete user
  app.delete(`/api/user/:id`, async (req, res) => {
    const { id } = req.params;
    console.log(id);

    db.get('users')
      .remove({ id })
      .write()

    return res.status(202).send({
      error: false
    });
  });
}