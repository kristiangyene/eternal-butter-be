
// Server setup.
const app = require('express')();
const http = require('http').Server(app);

// temporary cors fix.
const io = require('socket.io')(http, {
  cors: {
    origin: '*',
  }
});
const bodyParser = require('body-parser');

app.use(bodyParser.json());

// Add Access Control Allow Origin headers.
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

// Add API endpoints.
require('./routes/UserRoutes')(app);

if (process.env.NODE_ENV === 'production') {
  app.use(express.static('front/build'));

  const path = require('path');
  app.get('*', (req,res) => {
    res.sendFile(path.resolve(__dirname, 'front', 'build', 'index.html'));
  });
}

// Start listening to API clients.
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log(`app running on port ${PORT}`)
});

// Start listening for socket clients
http.listen(process.env.PORT || 4000);
console.log(`Started on port ${4000}`);

// Setup socket.io
io.on('connection', socket => {
  const username = socket.handshake.query.username;
  console.log(`${username} connected`);

  socket.on('client:message', data => {
    console.log(`${data.username}: ${data.message}`);

    // message received from client, now broadcast it to everyone else
    socket.broadcast.emit('server:message', data);
  });

  socket.on('disconnect', () => {
    console.log(`${username} disconnected`);
  });
});